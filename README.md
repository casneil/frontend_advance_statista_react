# Frontend Exercise Statista React

The aim of this exercise was to:

1. Create a user interface for displaying search results.
2. The results should at least include title and description.
3. Show the number of results for the given keyword (in this case “Statista”).
4. Keep usability and accessibility in mind. Code for real world conditions.

---

### Features

- View results data for search term.
- Title, description, date, image infomation rendered on page.
- Total number of results for given search term.
- Ability to sort results by date or relevance.
- Pagination

---

### How to run

cd into project and run the follwing command:

1. `npm install`
2. `npm start`

---

#### Screenshots

![picture](public/screenshots/screenshot1.png)
![picture](public/screenshots/screenshot2.png)
![picture](public/screenshots/screenshot3.png)

[View Vue version](https://bitbucket.org/casneil/frontend_advance_statista/src/master/)
