import "./App.css";

// Components
import { SearchBar, SearchResults } from "./components";

const App = () => {
  return (
    <div>
      <SearchBar />
      <SearchResults />
    </div>
  );
};

export default App;
