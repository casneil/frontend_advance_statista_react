interface IResultImages {
  width: number;
  src: string;
}

export interface IResults {
  identifier: number;
  title: string;
  link: string;
  subject: string;
  description: string;
  date: string;
  image_url: string;
  teaser_image_urls: Array<IResultImages>;
}
