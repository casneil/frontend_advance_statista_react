import { useState, useEffect } from "react";

//Components
import { Card, Pagination } from "../index";
// Contracts
import { IResults } from "../../contracts";

const SearchResults: React.FC = () => {
  const [searchResults, setSearchResults] = useState<IResults[]>([]);
  const [sortByDateDesc, setSortByDateDesc] = useState<string>("Relevance");
  const [loading, setLoading] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [resultsPerPage] = useState<number>(20);

  // Get current results
  const indexOfLastResult = currentPage * resultsPerPage;
  const indexOfFirstResult = indexOfLastResult - resultsPerPage;
  const currentResult = searchResults.slice(indexOfFirstResult, indexOfLastResult);

  useEffect(() => {
    const getResults = async () => {
      /* Built URL using javascript's URL API. Although this is not needed currently, it could be usefull for later use.
         For more advance queries with query params use the searchParams.append method e.g ->  endPoint.searchParams.append("term", "statista");
         Reference = https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
       */
      const endPoint = new URL("https://cdn.statcdn.com/static/application/search_results.json");
      try {
        const request = await fetch(endPoint.toString());
        setLoading(true);
        const response = await request.json();
        sortByDateDesc === "Relevance"
          ? setSearchResults(response.items)
          : setSearchResults(
              response.items.sort(
                (resultA: IResults, resultB: IResults) =>
                  parseInt(resultB.date.replace(/\-/g, "")) -
                  parseInt(resultA.date.replace(/\-/g, ""))
              )
            );
        setLoading(false);
      } catch (error) {
        // Simple error handling
        window.alert("Something went wrong. Please try again later");
      }
    };
    getResults();
    window.scrollTo(0, 0);
  }, [sortByDateDesc, currentPage]);

  return (
    <section className="searchApp__results ">
      <div className="panelCard padding-all-20">
        <h2 className="sectionRooftitle">
          {loading ? "loading..." : `${searchResults.length} Results`}
        </h2>
        <p>Sorted By:</p>
        <select value={sortByDateDesc} onChange={(event) => setSortByDateDesc(event.target.value)}>
          <option value="Relevance">Relevance</option>
          <option value="Date">Date</option>
        </select>
      </div>
      {currentResult.map((result, index) => (
        <Card results={result} key={`card-${index} ${result.identifier}`} />
      ))}
      <Pagination
        totalResults={searchResults.length}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        resultsPerPage={resultsPerPage}
      />
    </section>
  );
};

export default SearchResults;
