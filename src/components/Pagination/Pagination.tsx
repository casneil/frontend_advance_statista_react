import { FC } from "react";

//Types
type propType = {
  totalResults: number;
  currentPage: number;
  resultsPerPage: number;
  setCurrentPage: Function;
};

const Pagination: FC<propType> = ({
  totalResults,
  setCurrentPage,
  currentPage,
  resultsPerPage,
}) => {
  const allPages = Math.ceil(totalResults / resultsPerPage);

  const handlePageIncrement = () => {
    currentPage < allPages && setCurrentPage((prevCount: number) => prevCount + 1);
  };

  const handlePageDecrement = () => {
    currentPage > 1 && setCurrentPage((prevCount: number) => prevCount - 1);
  };

  return (
    <div className="pagination paginationRound searchPagination">
      <div className="paginationContainer row center">
        <div
          aria-label="Previous page"
          className="paginationRound__circlePrev font-size-xl circle row"
          onClick={handlePageDecrement}
        >
          <i className="fa fa-angle-left" />
        </div>

        <span aria-label="Page count" className="margin-left-20 font-size-l text-bold">
          {`${currentPage}`}
        </span>
        <span aria-label="All pages" className="margin-right-20 font-size-l text-color--gray">
          {` / ${allPages}`}
        </span>
        <div
          aria-label="Next page"
          className="paginationRound__circleNext font-size-xl circle row"
          onClick={handlePageIncrement}
        >
          <i className="fa fa-angle-right" />
        </div>
      </div>
    </div>
  );
};

export default Pagination;
