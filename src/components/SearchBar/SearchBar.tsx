import { useState } from "react";

const SearchBar: React.FC = () => {
  const [searchTerm, setSearchTerm] = useState<string>("statista");

  return (
    <div className="pos-relative searchApp__container">
      <input
        value={searchTerm}
        placeholder="Search for statistics"
        type="text"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => setSearchTerm(event.target.value)}
      />
      <button type="submit" className="button button--primary searchApp__submitButton">
        Search
      </button>
    </div>
  );
};

export default SearchBar;
