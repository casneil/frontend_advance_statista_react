import { FC } from "react";

import { format, parseISO } from "date-fns";
//Contracts
import { IResults } from "../../contracts";
//PropTypes
type propType = {
  results: IResults;
};
const Card: FC<propType> = ({ results }) => {
  const { date, description, teaser_image_urls, title } = results;
  return (
    <div className="panelCard panelCard--hover  padding-all-20 margin-top-15">
      <div className="flex">
        <img src={teaser_image_urls[2].src} alt={title} />
        <h5 className="margin-left-10 text-color--primary font-size-sm">
          {format(parseISO(date), "MMM d, yyy")}
        </h5>
        <div className="margin-left-5 margin-right-5">|</div>
        <h5 className="font-size-l">{title.split(" ")[0].replace(":", "")}</h5>
        <h4 className="font-size-l margin-top-5 margin-bottom-5">{title}</h4>
        <p>{description}</p>
      </div>
    </div>
  );
};

export default Card;
